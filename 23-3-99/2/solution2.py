from bs4 import BeautifulSoup

def process(name):
    with open(name,'r') as fp :
        text  = fp.read()
    soup = BeautifulSoup(text,'lxml')
    a_list = soup.select("a")
    return len(a_list)