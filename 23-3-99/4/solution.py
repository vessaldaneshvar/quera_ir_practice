class Piece:
    def __init__(self,sort,color,position : tuple):
        self.sort = sort
        self.color = color
        self.position = position


class Board:
    def __init__(self):
	    self.position = {(-10,-10):Piece("K", "white", (-10, -10)),(10,10):Piece("K", "black", (10, 10))}

    def add(self, piece):
        if (piece.position in self.position) or (piece.sort == "K") :
            print("invalid query")
        else:
            self.position[piece.position] = piece

    def remove(self, position): 
        if position in self.position:
            if self.position[position].sort != "K":
                self.position.pop(position)
            else:
                print("invalid query")
        else:
            print("invalid query")
            

    def move(self, piece, position2):
        if position2 in self.position:
            if (self.position[position2].color != piece.color) and (self.position[position2].sort == "P"):
                self.remove(position2)
                self.remove(piece.position)
                piece.position = position2
                self.add(piece)
            else:
                print("invalid query")
        else:
            self.remove(piece.position)
            piece.position = position2
            self.add(piece)

    def is_mate(self, color):
        for _ , piece in self.position.items():
            if (piece.sort == "K") and (piece.color==color):
                position = piece.position
                break
        i,j = position
        list_check = [(i+1,j), (i + 1 , j + 1),(i+1,j-1),(i,j+1),(i,j-1),(i-1,j+1),(i-1,j),(i-1,j-1)]
        for position_check in list_check:
            if position_check in self.position:
                if (self.position[position_check].sort == "P") and (self.position[position_check].color != color):
                    return True
        return False
