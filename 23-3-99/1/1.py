def fruits(tuple_of_fruits):
    result = {}
    for fruit in tuple_of_fruits :
       if (fruit['shape'] == "sphere") and (fruit['mass'] >=300) and (fruit['mass'] <=600) and (fruit['volume'] >=100) and (fruit['volume'] <=500) :
           result[fruit['name']]  = result.get(fruit['name'],0) + 1
    return result
